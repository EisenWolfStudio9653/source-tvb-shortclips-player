//
//  WebViewQALoginViewController.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/20/24.
//

import UIKit
import WebKit

class WebViewQALoginViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSubview(frame: self.view.frame)
        
        self.establishWebViewRequest()
    }
    
    private lazy var loginWebView: WKWebView = {
        let webViewConfig = WKWebViewConfiguration()
        
        let webView = WKWebView(
            frame: CGRect.zero,
            configuration: webViewConfig
        )
        
        webView.navigationDelegate = self
        
        return webView
    }()
}

extension WebViewQALoginViewController {
    private func setupSubview(frame: CGRect) {
        self.view.addSubview(self.loginWebView)
        
        AutoLayoutUI.manager.fillPredecessor(for: self.loginWebView)
    }
    
    private func establishWebViewRequest() {
//        guard let requestURL = URL(string: "https://qa-www.mytvsuper.com/shorts") else {
//            return
//        }
        
        guard let requestURL = URL(string: "https://scoopplus:Scoop_qa@qa-www.mytvsuper.com/tc/scoopplus/shorts") else {
            return
        }
        
        let request = URLRequest(
            url: requestURL,
            timeoutInterval: TimeInterval(10)
        )
        
        
        self.loginWebView.load(request)
    }
    
    private func evaluateRequestURL(for requestURL: URL) -> Bool {
        
        return true
    }
}

extension WebViewQALoginViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("<EWS> WebViewQALoginViewController -> WKNavigationDelegate => didFinish")
        
        ShortClipsWebView.manager.saveCookie()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        switch navigationAction.navigationType {
        case WKNavigationType.backForward:
            debugPrint("<EWS> Type: backForward")
        case WKNavigationType.formResubmitted:
            debugPrint("<EWS> Type: formResubmitted")
        case WKNavigationType.formSubmitted:
            debugPrint("<EWS> Type: formSubmitted")
        case WKNavigationType.linkActivated:
            debugPrint("<EWS> Type: linkActivated")
        case WKNavigationType.reload:
            debugPrint("<EWS> Type: reload")
        case WKNavigationType.other:
            debugPrint("<EWS> Type: other")
        
        @unknown default:
            debugPrint("<EWS> Type: @unknown default")
        }
        
        
        if let targetURL = navigationAction.request.url {
            debugPrint("<EWS> WebViewQALoginViewController -> decidePolicyForNavigationAction => targetURL: \(targetURL)")
            
            if self.evaluateRequestURL(for: targetURL) == true {
                decisionHandler(WKNavigationActionPolicy.cancel)
                return
            }
        }
        
        let isMainFrame = navigationAction.targetFrame?.isMainFrame ?? false
        
        if isMainFrame == false {
            decisionHandler(WKNavigationActionPolicy.allow)
            return
        }
        
        
        
        
        if navigationAction.navigationType == WKNavigationType.reload {
            decisionHandler(WKNavigationActionPolicy.cancel)
        } else {
            decisionHandler(WKNavigationActionPolicy.allow)
        }
        
        
        
    }
    
}
