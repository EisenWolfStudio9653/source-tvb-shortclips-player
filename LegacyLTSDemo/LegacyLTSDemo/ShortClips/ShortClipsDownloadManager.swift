//
//  ShortClipsDownloadManager.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 3/12/24.
//

import Foundation
//import AVFoundation
//import HLSion


final class ShortClipsDownload: NSObject {
    private override init() {}
    public static let manager = ShortClipsDownload()
    
    private var downloadSessionsContainer = [ShortClipDownloadSession]()
}

extension ShortClipsDownload {
    public func reset() {
        for entry in self.downloadSessionsContainer {
            entry.session.cancelDownload()
            do {
                try entry.session.deleteAsset()
            } catch {
                debugPrint("<EWS> ShortClipsDownload -> reset => deleteAsset Error")
            }
        }
    }
    
    public func establishDownloadSession(shortClipID: String, sessionURL: URL) {
        let fetchingRequest = URLRequest(
            url: sessionURL,
            timeoutInterval: 10
        )
        
        let fetchingConfiguration = URLSessionConfiguration()
        
        let session = URLSession(
            configuration: fetchingConfiguration, 
            delegate: self, 
            delegateQueue: OperationQueue.main
        )
        
        let dataTask = URLSession.shared.dataTask(with: fetchingRequest) { fetchedData, response, error in
            guard let fetchedData = fetchedData,
                  error == nil
            else {
                debugPrint("<EWS> => URLSession.shared.dataTask Error")
                return
            }
            
            debugPrint("<EWS> => Video URL: \(shortClipID)")
            debugPrint("<EWS> => M3U8 PlayList:")
            debugPrint("<EWS> => \(String(data: fetchedData, encoding: String.Encoding.utf8) ?? "M3U8 Error")")
            
            
        }
        
        dataTask.resume()
        
        return
        
        
        
        let testM3U8Decoder = M3U8Decoder()
        
        testM3U8Decoder.decode(MasterPlaylist.self, from: sessionURL) { result in
            switch result {
            case let Result.success(playList):
                debugPrint("<EWS> Segments: \(playList)")
                
            case let Result.failure(error):
                debugPrint("<EWS> error: \(error.localizedDescription)")
            }
        }
        
        return
        
        let hlsion = HLSion(url: sessionURL, name: shortClipID)
    
        hlsion.download { progress in
            debugPrint("<EWS> ShortClipsDownload -> establishDownloadSession => progress: \(progress)")
            if progress >= 0.5 {
                hlsion.pause()
            }
            
        }
        .finish { path in
            debugPrint("<EWS> ShortClipsDownload -> establishDownloadSession => Completed @<\(path)>")
        }
        .onError { error in
//            debugPrint("<EWS> ShortClipsDownload -> establishDownloadSession => HLSion Error: \(error.localizedDescription)")
        }
        
        let downloadSession = ShortClipDownloadSession(
            sessionID: shortClipID,
            session: hlsion
        )
        
        self.downloadSessionsContainer.append(downloadSession)
        
        
    }
    
    public func retrieveDownloadedSessionURL(shortClipID: String) -> URL? {
        debugPrint("<EWS> ShortClipsDownload -> establishDownloadSession")
        let session = self.downloadSessionsContainer.first { entry in
            entry.sessionID == shortClipID
        }
        
        guard let session = session else {
            debugPrint("<EWS> => No DownloadSession Matched shortClipID")
            return nil
        }
        
        guard let localURL = session.session.localUrl else {
            debugPrint("<EWS> => Session Not Completed")
            return nil
        }
        
        return localURL
        
    }
}

extension ShortClipsDownload: URLSessionTaskDelegate {
    
}

struct ShortClipDownloadSession {
    let sessionID: String
    let session: HLSion
}


//final class ShortClipsDownload: NSObject {
//    
//    private var mediaSelectionMap = [AVAssetDownloadTask: AVMediaSelection]()
//    
//    private var downloadSession: AVAssetDownloadURLSession!
//    
//    public func downloadAVAsset(videoPath: String) {
//        debugPrint("<EWS> ShortClipsDownloadManager -> downloadAVAsset")
//        guard let resourceURL = URL(string: videoPath) else {
//            debugPrint("<EWS> ShortClipsDownloadManager -> downloadAVAsset => URL Error")
//            return
//        }
//        
//        let downloadIdentifier = UUID().uuidString
//        let sessionConfiguration = URLSessionConfiguration.background(withIdentifier: downloadIdentifier)
//        
//        self.downloadSession = AVAssetDownloadURLSession(
//            configuration: sessionConfiguration,
//            assetDownloadDelegate: self,
//            delegateQueue: OperationQueue.main
//        )
//        
//        let avURLAsset = AVURLAsset(url: resourceURL)
//        
//        let downloadTask = self.downloadSession.makeAssetDownloadTask(
//            asset: avURLAsset,
//            assetTitle: downloadIdentifier,
//            assetArtworkData: nil,
//            options: nil
//        )
//        
//        downloadTask?.resume()
//    }
//    
//    public func restoreDownloads(downloadIdentifier: String) {
//        let sessionConfiguration = URLSessionConfiguration.background(withIdentifier: downloadIdentifier)
//        
//        self.downloadSession = AVAssetDownloadURLSession(
//            configuration: sessionConfiguration,
//            assetDownloadDelegate: self,
//            delegateQueue: OperationQueue.main
//        )
//        
//        self.downloadSession.getAllTasks { tasksArray in
//            for task in tasksArray {
//                guard let downloadTask = task as? AVAssetDownloadTask else {
//                    break
//                }
//                
//                let asset = downloadTask.urlAsset
//            }
//        }
//        
//    }
//    
//    
//    
//}

//extension ShortClipsDownload {
//    private func nextMediaSelection(_ asset: AVURLAsset) -> (mediaSelectionGroup: AVMediaSelectionGroup?, mediaSelectionOption: AVMediaSelectionOption?) {
//        
//        guard let assetCache = asset.assetCache else {
//            return (nil, nil)
//        }
//        
//        for characteristic in [AVMediaCharacteristic.audible, AVMediaCharacteristic.legible] {
//            if let mediaSelectionGroup = asset.mediaSelectionGroup(forMediaCharacteristic: characteristic) {
//                let savedOptions = assetCache.mediaSelectionOptions(in: mediaSelectionGroup)
//                
//                if savedOptions.count < mediaSelectionGroup.options.count {
//                    for option in mediaSelectionGroup.options {
//                        if savedOptions.contains(option) == false {
//                            return (mediaSelectionGroup, option)
//                        }
//                    }
//                }
//            }
//        }
//        
//        return (nil, nil)
//    }
//    
//}

//extension ShortClipsDownload: AVAssetDownloadDelegate {
//    
//    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didLoad timeRange: CMTimeRange, totalTimeRangesLoaded loadedTimeRanges: [NSValue], timeRangeExpectedToLoad: CMTimeRange) {
//        debugPrint("<EWS> ShortClipsDownloadManager -> AVAssetDownloadDelegate -> didLoadTimeRange")
//        var percentComplete = 0.0
//        
//        for value in loadedTimeRanges {
//            let loadedTimeRange = value.timeRangeValue
//            
//            percentComplete += loadedTimeRange.duration.seconds / timeRangeExpectedToLoad.duration.seconds
//        }
//        
//        percentComplete *= 100
//        
//        debugPrint("<EWS> => \(session.configuration.description) Progress => \(percentComplete)%")
//        
//        if percentComplete >= 30.0 {
//            session.getAllTasks { tasksArray in
//                for task in tasksArray {
//                    guard let downloadTask = task as? AVAssetDownloadTask else {
//                        break
//                    }
//                    
//                    downloadTask.cancel()
//                    
//                    debugPrint("<EWS> => \(session.configuration.description) Paused")
//                }
//            }
//        }
//    }
//    
//    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
//        debugPrint("<EWS> ShortClipsDownloadManager -> AVAssetDownloadDelegate -> didFinishDownloadingToLocation")
//        
//        UserDefaults.standard.set(location.relativePath, forKey: "assetPath")
//    }
//    
//    func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didResolve resolvedMediaSelection: AVMediaSelection) {
//        debugPrint("<EWS> ShortClipsDownloadManager -> AVAssetDownloadDelegate -> didResolveResolvedMediaSelection")
//        
//        
//        self.mediaSelectionMap[assetDownloadTask] = resolvedMediaSelection
//    }
//    
//    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
//        debugPrint("<EWS> ShortClipsDownloadManager -> AVAssetDownloadDelegate -> didCompleteWithError")
//        
//        guard error == nil else {
//            debugPrint("<EWS> => \(error.debugDescription)")
//            return
//        }
//        
//        guard let task = task as? AVAssetDownloadTask else {
//            return
//        }
//        
//        let mediaSelectionPair = self.nextMediaSelection(task.urlAsset)
//        
//        if let group = mediaSelectionPair.mediaSelectionGroup, 
//            let option = mediaSelectionPair.mediaSelectionOption {
//            
//            guard let originalMediaSelection = self.mediaSelectionMap[task] else {
//                return
//            }
//            
//            if let mediaSelection = originalMediaSelection.mutableCopy() as? AVMutableMediaSelection {
//                mediaSelection.select(option, in: group)
//                
//                let options = [AVAssetDownloadTaskMediaSelectionKey: mediaSelection]
//                
//                let task = self.downloadSession.makeAssetDownloadTask(
//                    asset: task.urlAsset,
//                    assetTitle: UUID().uuidString,
//                    assetArtworkData: nil,
//                    options: options
//                )
//                
//                task?.resume()
//            }
//            
//        } else {
//            // All media selection downloads complete
//        }
//    }
//}
    
