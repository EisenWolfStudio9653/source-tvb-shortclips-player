//
//  ShortClipsNavigationViewController.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import UIKit

class ShortClipsNavigationViewController: UIViewController {
    
    private var controllerView: ShortClipsNavigationView?
    private var controllerModel: ShortClipsNavigationModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Short Clips"
//        self.navigationController?.navigationBar.prefersLargeTitles = true
//        self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.always
        
        self.controllerView = ShortClipsNavigationView(frame: self.view.frame, controller: self)
        self.controllerModel = ShortClipsNavigationModel(viewController: self)

        guard let controllerView = self.controllerView else {
            return
        }
        
        self.view = controllerView
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        debugPrint("<EWS> ShortClipsNavigationViewController -> viewDidDisappear")
        self.controllerView?.resetShortClipsPagingView()
        self.controllerView = nil
        self.controllerModel = nil
        
        ShortClipsResource.manager.reset()
        ShortClipsDownload.manager.reset()
        
        NotificationCenter.default.post(
            name: Notification.Name("MasterKillSwitch"),
            object: nil
        )
    }
}
