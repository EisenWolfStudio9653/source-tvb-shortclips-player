//
//  ShortClipsPlayerManager.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/23/24.
//

import Foundation
import AVKit

final class ShortClipsPlayer {
    private init() {}
    public static let manager = ShortClipsPlayer()
    
    private var sharedShortClipPlayersContainer = [ShortClipVideoPlayer]()
}

extension ShortClipsPlayer {
    private func releaseCacheIfNeeded() {
        if self.sharedShortClipPlayersContainer.count > 20 {
            debugPrint("<EWS> ShortClipsPlayerManager -> releaseCacheIfNeeded")
            debugPrint("<EWS> => Container Size: \(self.sharedShortClipPlayersContainer.count)")
            
            self.sharedShortClipPlayersContainer.removeFirst()
        }
    }
}

extension ShortClipsPlayer {
    public func append(preloadedPlayer: ShortClipVideoPlayer) {
        if self.sharedShortClipPlayersContainer.contains(where: { entry in
            entry.retrieveID() == preloadedPlayer.retrieveID()
        }) == false {
            self.sharedShortClipPlayersContainer.append(preloadedPlayer)
        }
    }
    
    public func retrievePreloadedPlayer(cacheID: String) -> ShortClipVideoPlayer? {
        let result = self.sharedShortClipPlayersContainer.first { entry in
            entry.retrieveID() == cacheID
        }
        
        return result
    }
    
    public func play(player: ShortClipVideoPlayer) {
        debugPrint("<EWS> ShortClipsPlayerManager -> Play")
        
        self.pauseAll()
        
        if self.sharedShortClipPlayersContainer.contains(where: { entry in
            entry.retrieveID() == player.retrieveID()
        }) == false {
            self.sharedShortClipPlayersContainer.append(player)
            debugPrint("<EWS> => Player DNE, Appended")
            
            self.releaseCacheIfNeeded()
        } else {
            debugPrint("<EWS> => Player Found")
        }
        
//        if self.sharedShortClipPlayersContainer.contains(player) == false {
//            self.sharedShortClipPlayersContainer.append(player)
//            debugPrint("<EWS> => Player DNE, Appended")
//        } else {
//            debugPrint("<EWS> => Player Found")
//        }
        
        player.retrievePlayer().play()
    }
    
    public func replay(player: ShortClipVideoPlayer) {
        debugPrint("<EWS> ShortClipsPlayerManager -> replay")
        self.pauseAll()
        
        if self.sharedShortClipPlayersContainer.contains(where: { entry in
            entry.retrieveID() == player.retrieveID()
        }) == false {
            self.sharedShortClipPlayersContainer.append(player)
            debugPrint("<EWS> => Player DNE, Appended")
            
            self.releaseCacheIfNeeded()
        } else {
            player.retrievePlayer().seek(to: CMTime.zero)
            debugPrint("<EWS> => Player Found")
        }
        
//        if self.sharedShortClipPlayersContainer.contains(player) == false {
//            self.sharedShortClipPlayersContainer.append(player)
//            debugPrint("<EWS> => Player DNE, Appended")
//        } else {
//            player.seek(to: CMTime.zero)
//            debugPrint("<EWS> => Player Found")
//        }
        
        player.retrievePlayer().play()
        
    }
    
    public func pause(player: ShortClipVideoPlayer) {
        if self.sharedShortClipPlayersContainer.contains(where: { entry in
            entry.retrieveID() == player.retrieveID()
        }) == true {
            player.retrievePlayer().pause()
        }
        
//        if self.sharedShortClipPlayersContainer.contains(player) == true {
//            player.pause()
//        }
    }
    
    public func pauseAll() {
        for entry in self.sharedShortClipPlayersContainer {
            entry.retrievePlayer().pause()
        }
    }
    
    
}
