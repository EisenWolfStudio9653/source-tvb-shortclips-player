//
//  ShortClipsNavigationModel.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import Foundation

class ShortClipsNavigationModel {
    private let viewController: ShortClipsNavigationViewController
    
    init(viewController: ShortClipsNavigationViewController) {
        self.viewController = viewController
    }
}

extension ShortClipsNavigationModel {
    
}
