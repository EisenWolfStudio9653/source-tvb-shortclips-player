//
//  PreloadedShortClip.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import UIKit
import AVKit

struct PreloadedShortClipVideo {
    public let cacheID: String
    
    public var cachedPlayerItem: AVPlayerItem?
}

extension PreloadedShortClipVideo: Identifiable {
    var id: String {
        return self.cacheID
    }
}

struct PreloadedShortClipThumbnail {
    public let cacheID: String
 
    public var cachedThumbnailImage: UIImage?
}

extension PreloadedShortClipThumbnail: Identifiable {
    var id: String {
        return self.cacheID
    }
}
