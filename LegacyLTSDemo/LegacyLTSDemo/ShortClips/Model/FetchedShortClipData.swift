//
//  FetchedShortClipData.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import Foundation

struct FetchedShortClipData {
    public let cacheID: String
    
    public let title: String
    public let shortClipID: String
    public let videoURL: String
    public let thumbnailImageURL: String
}

extension FetchedShortClipData: Identifiable {
    var id: String {
        return self.cacheID
    }
}

extension FetchedShortClipData: Hashable, Equatable {
    var hashValue: Int {
        get {
            return self.cacheID.hashValue
        }
    }
    
//    func hash(into hasher: inout Hasher) {
//        
//    }
    
    static func ==(left:FetchedShortClipData, right:FetchedShortClipData) -> Bool {
        return left.cacheID == right.cacheID
    }
}
