//
//  ShortClipsResourceClipsManager.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import Foundation
import AVKit

final class ShortClipsResource {
    private init() {}
    public static let manager = ShortClipsResource()
    
    public var fetchedShortClipsContainer = [FetchedShortClipData]()
    
    private var shortClipsCacheContainerSize = 5
    private var fetchedShortClipsGroupIndex = 1
    
//    private var preloadedShortClipVideosContainer = [PreloadedShortClipVideo]()
    private var preloadedShortClipThumbnailsContainer = [PreloadedShortClipThumbnail]()

}

extension ShortClipsResource {
    public func initialize() {

        self.fetchNewClipsFromAPI {
            self.fetchNewClipsFromAPI {
                
                debugPrint("<EWS> ShortClipsResourceManager -> initialize => current fetched entries count: \(self.fetchedShortClipsContainer.count)")
                
                DispatchQueue.global().async {
                    for entry in self.fetchedShortClipsContainer {
                        self.preloadShortCLip(shortClipID: entry.videoURL, videoURL: entry.videoURL, imageURL: entry.thumbnailImageURL)
                        
                    }
                }
                
                
                self.postShortClipsContainerInitializedNotification()
                
            }

        }
 
    }
    
    public func preloadShortCLip(shortClipID: String, videoURL: String, imageURL: String) {
        debugPrint("<EWS> ShortClipsResourceManager -> preloadShortCLip")
        
        if ShortClipsPlayer.manager.retrievePreloadedPlayer(cacheID: videoURL) == nil {
            
            guard let url = URL(string: videoURL) else {
                return
            }
            
            ShortClipsDownload.manager.establishDownloadSession(shortClipID: shortClipID, sessionURL: url)
            
//            self.preloadShortClipVideo(shortClipID: shortClipID, path: videoURL) { fetchedVideo in
//                debugPrint("<EWS> ShortClipsResourceManager -> preloadShortCLip => Video Loaded")
//                
//                let preloadedShortClipPlayer = ShortClipVideoPlayer(
//                    playerItem: fetchedVideo,
//                    playerID: videoURL
//                )
//                
//                DispatchQueue.main.async {
//                    ShortClipsPlayer.manager.append(preloadedPlayer: preloadedShortClipPlayer)
//                }
//                
//            }

        }

        if self.fetchPreloadedShortChipThumbnail(cacheID: videoURL) == nil {
            self.preloadShortClipThumbnailImage(path: imageURL) { fetchedImage in
                debugPrint("<EWS> ShortClipsResourceManager -> preloadShortCLip => Thumbnail Loaded")
                
                let preloadedThumbnail = PreloadedShortClipThumbnail(
                    cacheID: videoURL,
                    cachedThumbnailImage: fetchedImage
                )
                
                self.preloadedShortClipThumbnailsContainer.append(preloadedThumbnail)
                
                if self.preloadedShortClipThumbnailsContainer.count > 20 {
                    self.preloadedShortClipThumbnailsContainer.removeFirst()
                }
            }
        }
        
    }
    
    public func checkFetchedShortClipsContainerStatus(currentIndex: Int) {
        debugPrint("<EWS> ShortClipsResourceManager -> checkFetchedClipsContainerStatus")
        
        self.fetchNewClipsFromAPI {
            
            DispatchQueue.global().async {
                for entry in self.fetchedShortClipsContainer {
                    self.preloadShortCLip(shortClipID: entry.videoURL, videoURL: entry.videoURL, imageURL: entry.thumbnailImageURL)
                }
            }
            
            self.postShortClipsContainerUpdatedNotification()
            
        }
        
    }
    
    public func removeDuplicatedShortClips(from inputContainer: [FetchedShortClipData]) -> [FetchedShortClipData] {
        var uniqueContainer = [FetchedShortClipData]()
            for entry in inputContainer {
                if !uniqueContainer.contains(where: {$0.cacheID == entry.cacheID }) {
                    uniqueContainer.append(entry)
                }
            }
            return uniqueContainer
    }
    
//    public func fetchPreloadedShortChipVideo(cacheID: String) -> AVPlayerItem? {
//        debugPrint("<EWS> ShortClipsResourceManager -> fetchPreloadedShortChipVideo")
//        
//        let result: AVPlayerItem?
//        if let cachedShortClip = self.preloadedShortClipVideosContainer.first(where: { entry in
//            entry.cacheID == cacheID
//        }) {
//            debugPrint("<EWS> => Video Cache Fount")
//            result = cachedShortClip.cachedPlayerItem
//            
////            self.preloadedShortClipVideosContainer.removeAll { entry in
////                entry.cacheID == cacheID
////            }
//        } else {
//            debugPrint("<EWS> => Video Cache DNE")
//            result = nil
//        }
//        
//        return result
//    }
    
    public func fetchPreloadedShortChipThumbnail(cacheID: String) -> UIImage? {
//        debugPrint("<EWS> ShortClipsManager -> fetchPreloadedShortChipThumbnail for: <\(cacheID)>")
        
        if let cachedShortClip = self.preloadedShortClipThumbnailsContainer.first(where: { entry in
            entry.cacheID == cacheID
        }) {
//            let restoredEntry = PreloadedShortClipThumbnail(
//                cacheID: cachedShortClip.cacheID,
//                cachedThumbnailImage: cachedShortClip.cachedThumbnailImage
//            )
            
//            self.preloadedShortClipThumbnailsContainer.removeAll { entry in
//                entry.cacheID == cacheID
//            }
            
//            self.preloadedShortClipThumbnailsContainer.append(restoredEntry)
            
//            debugPrint("<EWS> => Cache Found")
            return cachedShortClip.cachedThumbnailImage
        } else {
//            debugPrint("<EWS> => Cache DNE")
            return nil
        }
    }
    
    public func reset() {
        self.fetchedShortClipsContainer.removeAll()
        self.preloadedShortClipThumbnailsContainer.removeAll()
//        self.preloadedShortClipVideosContainer.removeAll()
        self.fetchedShortClipsGroupIndex = 1
        
    }
    

}

extension ShortClipsResource {
    private func fetchNewClipsFromAPI(completionHandler: @escaping () -> Void) {
        debugPrint("<EWS> ShortClipsResourceManager -> fetchNewClipsFromAPI => with page: <\(self.fetchedShortClipsGroupIndex)>")
        
        guard let fetchingURL = URL(string: "https://www.mytvsuper.com/tc/scoopplus/api/short?page=\(self.fetchedShortClipsGroupIndex)&limit=\(self.shortClipsCacheContainerSize)")
        else {
            debugPrint("<EWS> => fetchingURL Error!!!")
            return
        }
        
//        guard let fetchingURL = URL(string: "https://scoopplus:Scoop_qa@qa-www.mytvsuper.com/tc/scoopplus/shorts/api/short?page=\(self.fetchedShortClipsGroupIndex)&limit=\(self.shortClipsCacheContainerSize)")
//        else {
//            debugPrint("<EWS> => fetchingURL Error!!!")
//            return
//        }
        
        
        
        var fetchingRequest = URLRequest(
            url: fetchingURL,
            timeoutInterval: 10
        )
        
        fetchingRequest.setValue("Mozilla/5.0 (iPhone; CPU iPhone OS 17_2_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 myTVSUPER/5.7.1 SuperApp", forHTTPHeaderField: "User-Agent")
        
        let dataTask = URLSession.shared.dataTask(with: fetchingRequest) { fetchedData, response, error in
            guard let fetchedData = fetchedData,
                    error == nil
            else {
                debugPrint("<EWS> => URLSession.shared.dataTask Error")
                return
            }
            
            guard let dictionary = try? JSONSerialization.jsonObject(with: fetchedData, options: []) as? [String: Any], 
                    let sublayer1 = dictionary["data"] as? [String: Any],
                    let sublayer2 = sublayer1["list"] as? [[String: Any]]
            else {
                return
            }
            
//            debugPrint("<EWS> => list: \(dictionary)")
            
            for shortClipData in sublayer2 {
                guard let videoURL = shortClipData["video_url"] as? String,
                        let shortClipIDInteger = shortClipData["article_id"] as? Int,
                        let videoTitle = shortClipData["article_title"] as? String,
                        let thumbnailImageContainer = shortClipData["video_cover"] as? [String: Any],
                      let imageURL = thumbnailImageContainer["large"] as? String 
                else {
                    return
                }
                
//                debugPrint("<EWS> => ClipID: \(shortClipIDInteger)")
//                debugPrint("<EWS> => ClipTitle: \(videoTitle)")
//                debugPrint("<EWS> => ClipURL: \(videoURL)")
//                debugPrint("<EWS> => ClipThumbnail: \(imageURL)")
                
                let fetchedShortClipData = FetchedShortClipData(
                    cacheID: videoURL,
                    title: videoTitle,
                    shortClipID: String(describing: shortClipIDInteger),
                    videoURL: videoURL,
                    thumbnailImageURL: imageURL
                )
                
                
                
                DispatchQueue.main.async {
                    
                    if self.fetchedShortClipsContainer.first(where: { entry in
                        entry.cacheID == videoURL
                    }) == nil {
                        self.fetchedShortClipsContainer.append(fetchedShortClipData)
                    } else {
                        debugPrint("<EWS> ShortClipsResourceManager -> fetchNewClipsFromAPI => Repeated Entry, NOP")
                    }
                    
                }
            }
            
            DispatchQueue.main.async {
                completionHandler()
            }
        }
        
        dataTask.resume()
        
        self.fetchedShortClipsGroupIndex += 1
    }
    
    private func preloadShortClipThumbnailImage(path: String, completionHandler: ((_ fetchedImage: UIImage?) -> Void)?) {
//        debugPrint("<EWS> ShortClipsManager -> preloadShortClipThumbnailImage")
        
        guard let imageURL = URL(string: path) else {
            completionHandler?(nil)
            return
        }
        
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: imageURL), let fetchedImage = UIImage(data: data) {
                
                DispatchQueue.main.async {
                    completionHandler?(fetchedImage)
                }
            }
        }
    }
    
    private func preloadShortClipVideo(shortClipID: String, path: String, completionHandler: ((_ fetchedPlayerItem: AVPlayerItem?) -> Void)?) {
        debugPrint("<EWS> ShortClipsResourceManager -> preloadShortClipVideo")
        
        guard let videoURL = URL(string: path) else {
            debugPrint("<EWS> => URL Error")
            completionHandler?(nil)
            return
        }
        
        let loadedAsset = AVAsset(url: videoURL)
        
        loadedAsset.loadValuesAsynchronously(forKeys: ["playable"]) {
            var error: NSError? = nil
            let status = loadedAsset.statusOfValue(forKey: "playable", error: &error)
            
            switch status {
            case AVKeyValueStatus.loaded:
//                debugPrint("<EWS> => Loaded")
                
                let loadedPlayerItem = AVPlayerItem(asset: loadedAsset)
                loadedPlayerItem.preferredForwardBufferDuration = 3.0
                
                DispatchQueue.main.async {
                    completionHandler?(loadedPlayerItem)
                }
            case AVKeyValueStatus.failed:
                debugPrint("<EWS> => Failed")
            case AVKeyValueStatus.cancelled:
                debugPrint("<EWS> => Canceled")
            default:
                debugPrint("<EWS> => Unknow State")
            }
        }
        
    }
    
    private func postShortClipsContainerUpdatedNotification() {
        NotificationCenter.default.post(
            name: Notification.Name("ShortClipsContainerUpdated"),
            object: nil
        )
    }
    
    private func postShortClipsContainerInitializedNotification() {
        NotificationCenter.default.post(
            name: Notification.Name("ShortClipsContainerInitialized"),
            object: nil
        )
    }
}
