//
//  ShortClipReusableCell.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/13/24.
//

import UIKit
import AVKit
import WebKit

typealias OnPlayerReady = () -> Void

class ShortClipReusableCell: UITableViewCell {
    
    private lazy var shortClipThumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.backgroundColor = UIColor.black
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        
        return imageView
    }()
    
    private lazy var shortClipVideoPlayerView: ShortClipVideoPlayerView = {
        let view = ShortClipVideoPlayerView(frame: self.frame)
        
        view.playerStatusUpdateDelegate = self
        
        return view
    }()
    
    private lazy var webviewOverlay: WKWebView = {
        let webViewConfig = WKWebViewConfiguration()
        
        let webView = WKWebView(
            frame: CGRect.zero,
            configuration: webViewConfig
        )
        
        webView.navigationDelegate = self
        
        webView.isOpaque = false
        webView.backgroundColor = UIColor.clear
        webView.scrollView.backgroundColor = UIColor.clear
        
        webView.scrollView.isScrollEnabled = false
        
        return webView
    }()
    
    private lazy var shortClipVideoPlayerProgressBar: UISlider = {
        let slider = UISlider()
        
        slider.minimumTrackTintColor = UIColor.orange
        slider.maximumTrackTintColor = UIColor.white
        slider.setThumbImage(UIImage(), for: UIControl.State.normal)
        slider.thumbTintColor = UIColor.orange
        slider.isUserInteractionEnabled = true
        slider.minimumValue = 0.0
        slider.value = 0.0
        slider.isContinuous = false
        
        slider.addTarget(self, action: #selector(self.shortClipVideoProgressBarHandler), for: UIControl.Event.valueChanged)
        self.shortClipVideoPlayerProgressBarObserver = slider.observe(
            \UISlider.isTracking,
             options: [
                NSKeyValueObservingOptions.old,
                NSKeyValueObservingOptions.new
             ],
             changeHandler: { _, changedValue in
                 guard let updatedValue = changedValue.newValue else {
                     return
                 }
                 
                 self.shortClipVideoPlayerProgressBarUpdated(status: updatedValue, currentValue: slider.value)
             })
        
        return slider
    }()
    
//    func panGesture(gesture:UIPanGestureRecognizer){
//        let currentPoint = gesture.location(in: self.shortClipVideoPlayerProgressBar)
//        let percentage = currentPoint.x / self.shortClipVideoPlayerProgressBar.bounds.size.width;
//        let delta = Float(percentage) *  (self.shortClipVideoPlayerProgressBar.maximumValue - self.shortClipVideoPlayerProgressBar.minimumValue)
//        let value = self.shortClipVideoPlayerProgressBar.minimumValue + delta
//        self.shortClipVideoPlayerProgressBar.setValue(value, animated: true)
//    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCell.SelectionStyle.none
        
        self.setupSubview()
        self.setupObservers()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.shortClipID = nil
        self.shortClipTitle = nil
        self.shortClipVideoURL = nil
        self.shortCLipThumbnailURL = nil
        self.shortClipThumbnailImageView.image = nil
        
        self.shortClipVideoPlayerView.cancelLoading()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        AutoLayoutUI.manager.fillPredecessor(for: self.shortClipVideoPlayerView)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.isPlayerReady = false
        self.shortClipVideoPlayerView.cancelLoading()
        
        self.shortClipThumbnailImageView.isHidden = false
        self.shortClipVideoPlayerView.isHidden = true
        self.webviewOverlay.isHidden = true
        self.shortClipVideoPlayerProgressBar.isHidden = true

    }
    
    public var onPlayerReady: OnPlayerReady?
    public var isPlayerReady = false
    
    // Video Progress
    private var shortClipVideoPlayerProgressBarObserver: NSKeyValueObservation?
    
    // ShortClip Parameters
    private var shortClipID: String?
    private var shortClipTitle: String?
    private var shortClipVideoURL: String?
    private var shortCLipThumbnailURL: String?

    private var shortClipPlayerItem: AVPlayerItem?


}

extension ShortClipReusableCell {
    private func setupSubview() {
        self.backgroundColor = UIColor.black
        
        self.addSubview(self.shortClipThumbnailImageView)
        AutoLayoutUI.manager.fillPredecessor(for: self.shortClipThumbnailImageView)
        
//        self.addSubview(self.shortClipVideoPlayerView.view)
//        self.shortClipVideoPlayerView.view.isHidden = true
//        AutoLayoutUI.manager.fillPredecessor(for: self.shortClipVideoPlayerView.view)
        
        self.addSubview(self.shortClipVideoPlayerView)
        AutoLayoutUI.manager.fillPredecessor(for: self.shortClipVideoPlayerView)
        self.shortClipVideoPlayerView.isHidden = true
        
        self.addSubview(self.webviewOverlay)
        AutoLayoutUI.manager.fillPredecessor(for: self.webviewOverlay)
        self.webviewOverlay.isHidden = true
        
        self.addSubview(self.shortClipVideoPlayerProgressBar)
        AutoLayoutUI.manager.placeAxises(
            for: self.shortClipVideoPlayerProgressBar,
            viewSize: CGSize(width: self.frame.width, height: 10),
            x: AutoLayoutUI.LayoutXAxis.center,
            xAnchor: self.centerXAnchor,
            xOffset: 0,
            y: AutoLayoutUI.LayoutYAxis.bottom,
            yAnchor: self.bottomAnchor,
            yOffset: -150
        )
        self.shortClipVideoPlayerProgressBar.isHidden = true

    }
    
    private func setupObservers() {
     
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(shortCLipDidReachEndHandler),
            name: AVPlayerItem.didPlayToEndTimeNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(masterKillSwitchPauseHandler),
            name: Notification.Name("MasterKillSwitch"),
            object: nil
        )
        
    }
    
    private func shortClipVideoPlayerProgressBarUpdated(status: Bool, currentValue: Float) {
        if status == true {
            debugPrint("<EWS> shortClipVideoPlayerProgressBarUpdated => currentValue: \(currentValue)")
        }
    }
    
    private func establishWebViewRequest(shortClipID: String) {
        
//        let javaScript = ""
//        
//        let userScript = WKUserScript(source: javaScript, injectionTime: WKUserScriptInjectionTime.atDocumentEnd, forMainFrameOnly: true)
        
//        let websiteDataStore = WKWebsiteDataStore.nonPersistent()
        
        ShortClipsWebView.manager.loadCookie()
        
//        let cookie = HTTPCookie(
//            properties: [
//                HTTPCookiePropertyKey.domain : "Any",
//                HTTPCookiePropertyKey.path : "Any",
//                HTTPCookiePropertyKey.name : "Any",
//                HTTPCookiePropertyKey.value : "Any",
//                HTTPCookiePropertyKey.secure : "Any",
//                HTTPCookiePropertyKey.expires : "Any"
//            ]
//        )
        
//        guard let cookie = cookie else {
//            return
//        }
        
//        guard let requestURL = URL(string: "https://qa-www.mytvsuper.com/tc/scoopplus/app-native-shorts/\(shortClipID)/") else {
//            return
//        }
        
        
//        self.webviewOverlay.load(request)
        
        
        
        
    }
    
    @objc
    private func shortCLipDidReachEndHandler() {
        NotificationCenter.default.post(
            name: Notification.Name("PlayNextShortClip"),
            object: nil
        )
    }
    
    @objc
    private func shortClipVideoProgressBarHandler() {
        debugPrint("<EWS> shortClipVideoProgressBarHandler => Progress: \(self.shortClipVideoPlayerProgressBar.value)")
        
    }
    
    @objc
    private func masterKillSwitchPauseHandler() {
        ShortClipsPlayer.manager.pauseAll()
    }
    
}

extension ShortClipReusableCell {
    
    public func updateCellData(title: String, shortClipID: String, videoURL: String, imageURL: String) {

        self.shortClipID = shortClipID
        self.shortClipVideoURL = videoURL
        self.shortClipTitle = title
        self.shortCLipThumbnailURL = imageURL

        // Thumbnail Image
        if let cachedShortClipThumbnail = ShortClipsResource.manager.fetchPreloadedShortChipThumbnail(cacheID: videoURL) {
            debugPrint("<EWS> => Cached Thumbnail Found, load from cache")
            
            self.shortClipThumbnailImageView.image = cachedShortClipThumbnail
            
        } else {
            debugPrint("<EWS> => Cached Thumbnail DNE, load from URL")
            
            if let imageURL = URL(string: imageURL) {
                DispatchQueue.global().async {
                    if let data = try? Data(contentsOf: imageURL),
                        let fetchedImage = UIImage(data: data) {
                        DispatchQueue.main.async {
                            self.shortClipThumbnailImageView.image = fetchedImage
                        }
                    }
                }
            } else {
                debugPrint("<EWS> => imageURL Error")
            }
        }
     
        
        // Video Player
        self.shortClipVideoPlayerView.updatePlayerViewData(videoPath: videoURL)
        
        // - MARK: WebView Overlay
//        self.establishWebViewRequest(shortClipID: shortClipID)
        
    }
    
    public func retrieveShortClipID() -> String? {
        return self.shortClipID
    }
    
    public func retrieveShortClipTitle() -> String? {
        return self.shortClipTitle
    }
    
    public func retrieveShortClipVideoURL() -> String? {
        return self.shortClipVideoURL
    }
    
    public func retrieveShortClipImageURL() -> String? {
        return self.shortCLipThumbnailURL
    }
    
    public func retrievePlayPauseStatus() -> Bool {
        let status: Bool
        
        if self.shortClipVideoPlayerView.currentPlaybackRate() == 0.0 {
            status = false
        } else {
            status = true
        }
        
        return status
    }
    
    public func play() {
        self.shortClipVideoPlayerView.play()
        
        self.shortClipThumbnailImageView.isHidden = true
        self.shortClipVideoPlayerView.isHidden = false
        self.shortClipVideoPlayerProgressBar.isHidden = false
        
    }
    
    public func pause() {
        self.shortClipVideoPlayerView.pause()
        
        self.shortClipThumbnailImageView.isHidden = false
        self.shortClipVideoPlayerView.isHidden = true
        self.shortClipVideoPlayerProgressBar.isHidden = true
    }
    
    public func replay() {
        self.shortClipVideoPlayerView.replay()
        
        self.shortClipThumbnailImageView.isHidden = true
        self.shortClipVideoPlayerView.isHidden = false
        self.shortClipVideoPlayerProgressBar.isHidden = false
    }
    
}

// - MARK: Delegate Functions
extension ShortClipReusableCell: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        self.webviewOverlay.isHidden = false
    }
    
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        if let url = navigationAction.request.url {
//            debugPrint("<EWS> ShortClipReusableCell -> decidePolicyForNavigationAction => URL: \(url)")
//        }
//        
//        decisionHandler(WKNavigationActionPolicy.allow)
//    }
    
}

extension ShortClipReusableCell: ShortClipVideoPlayerViewStatusUpdateDelegate {
    func onProgressUpdate(currentProgress current: Float, duration total: Float) {
//        debugPrint("<EWS> ShortClipReusableCell -> onProgressUpdate => current: \(current) total: \(total)")
        
        if current.isNaN == false
            && total.isNaN == false {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut) {
                self.shortClipVideoPlayerProgressBar.maximumValue = total
                self.shortClipVideoPlayerProgressBar.setValue(current, animated: true)
            }
        }
        
        
    }
    
    func onPlayItemStatusUpdate(status: AVPlayerItem.Status) {
        switch status {
        case AVPlayerItem.Status.readyToPlay:
            self.isPlayerReady = true
            self.onPlayerReady?()
            
            break
            
        case AVPlayerItem.Status.failed:
            debugPrint("<EWS> ShortClipReusableCell -> ShortClipVideoPlayerViewStatusUpdateDelegate -> onPlayItemStatusUpdate => failed")
            break
            
        case AVPlayerItem.Status.unknown:
            debugPrint("<EWS> ShortClipReusableCell -> ShortClipVideoPlayerViewStatusUpdateDelegate -> onPlayItemStatusUpdate => unknown")
            break
            
        @unknown default:
            debugPrint("<EWS> ShortClipReusableCell -> ShortClipVideoPlayerViewStatusUpdateDelegate -> onPlayItemStatusUpdate => @unknown default")
            break
        }
    }
    
}


class PlayerProgressBar: UIView {
    private lazy var bufferProgress: UIProgressView = {
        let view = UIProgressView(progressViewStyle: UIProgressView.Style.default)
        
        view.progressTintColor = UIColor.lightGray.withAlphaComponent(0.5)
        view.trackTintColor = UIColor.black
        
        view.progress = 0.0
        
        return view
    }()
    
    private lazy var playerProgress: UISlider = {
        let slider = UISlider()
        
        slider.minimumTrackTintColor = UIColor.orange
        slider.maximumTrackTintColor = UIColor.white
        slider.setThumbImage(UIImage(), for: UIControl.State.normal)
        slider.minimumValue = 0.0
        slider.value = 0.0
        slider.isContinuous = true
        
        slider.addTarget(self, action: #selector(self.videoProgressUpdatedHandler), for: UIControl.Event.valueChanged)
        self.shortClipVideoPlayerProgressBarObserver = slider.observe(
            \UISlider.isTracking,
             options: [
                NSKeyValueObservingOptions.old,
                NSKeyValueObservingOptions.new
             ],
             changeHandler: { progressSlider, didChange in
                 guard let didChange = didChange.newValue else {
                     return
                 }
                 
                 self.delegate?.videoProgressUpdated(status: didChange, value: progressSlider.value)
             })
        
        return slider
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var delegate: PlayerProgressBarDelegate?
    private var shortClipVideoPlayerProgressBarObserver: NSKeyValueObservation?
    
}

extension PlayerProgressBar {
    public func updateVideoProgress(playbackTime: Float, duration: Float) {
        self.bufferProgress
    }
    
    public func updateBufferProgress(bufferTime: Float, duration: Float) {
        
    }
}

extension PlayerProgressBar {

    
    @objc
    private func videoProgressUpdatedHandler() {
        
    }
}

protocol PlayerProgressBarDelegate {
    func videoProgressUpdated(status: Bool, value: Float)
}
