//
//  ShortClipsPagingView.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/18/24.
//

import UIKit

class ShortClipsPagingView: UIView {
    private let viewController: ShortClipsNavigationViewController
    
    private var isCurrentShortClipCellPaused = false
    
    // Paging Controls
    @objc
    dynamic var currentShortClipIndex = 0
    
    private var previousShortClipIndex = 0
 
    private var renderDataSource = [FetchedShortClipData]()


    private let reusableShortClipCellID = "reusableShortClipCellID"
    
    init(frame: CGRect, controller: ShortClipsNavigationViewController) {
        self.viewController = controller
        super.init(frame: frame)
        
        self.setupSubview(frame: frame)
        self.setupObservers()

        self.renderDataSource.removeAll()
        self.shortClipsContainerView.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        debugPrint("<EWS> ShortClipsPagingView -> deinit")
        self.currentShortClipIndex = 0
        
        NotificationCenter.default.removeObserver(self)
        self.removeObserver(self, forKeyPath: "currentShortClipIndex")
    }
    
    // Subview Components
    private let loadingOverlay: UILabel = {
        let label = UILabel()
        
        label.text = "Loading..."
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 36)
        label.numberOfLines = 1
        label.isHidden = true
        
        return label
    }()
    
    private lazy var shortClipsContainerView: UITableView = {
        let view = UITableView()
        
        view.backgroundColor = UIColor.black
        view.isPagingEnabled = true
        view.layer.removeAllAnimations()
        view.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
        view.showsVerticalScrollIndicator = false
        view.showsHorizontalScrollIndicator = false
        view.register(
            ShortClipReusableCell.self,
            forCellReuseIdentifier: self.reusableShortClipCellID
        )
        view.dataSource = self
        view.delegate = self
        
        return view
    }()
    
}

extension ShortClipsPagingView {
    private func setupSubview(frame: CGRect) {

        self.addSubview(self.shortClipsContainerView)
        self.addSubview(self.loadingOverlay)
        
        AutoLayoutUI.manager.fillPredecessor(for: self.shortClipsContainerView)
        AutoLayoutUI.manager.fillPredecessor(for: self.loadingOverlay)
    }
    
    private func setupObservers() {
        self.addObserver(
            self,
            forKeyPath: "currentShortClipIndex",
            options: [
                NSKeyValueObservingOptions.old,
                NSKeyValueObservingOptions.new
            ],
            context: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationBecomeActive),
            name: UIApplication.willEnterForegroundNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationEnterBackground),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.dataUpdatedHandler),
            name: Notification.Name("ShortClipsContainerUpdated"),
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.dataInitializedHandler),
            name: Notification.Name("ShortClipsContainerInitialized"),
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.pageNextHandler),
            name: Notification.Name("PlayNextShortClip"),
            object: nil
        )
        
    }
    
    @objc
    private func dataUpdatedHandler() {
        debugPrint("<EWS> ShortClipsPagingView -> dataUpdatedHandler")
        
        let updatedShortClipsContainer = ShortClipsResource.manager.fetchedShortClipsContainer
        ShortClipsResource.manager.fetchedShortClipsContainer.removeAll()
        
        self.renderDataSource += updatedShortClipsContainer
        var indexPathsContainer = [IndexPath]()
        
        for row in (self.renderDataSource.count - updatedShortClipsContainer.count)..<self.renderDataSource.count {
            indexPathsContainer.append(IndexPath.init(row: row, section: 0))
        }
        
        self.shortClipsContainerView.insertRows(at: indexPathsContainer, with: UITableView.RowAnimation.none)
        
        ShortClipsPlayer.manager.pauseAll()
        self.shortClipsContainerView.reloadData()
        
    }
    
    @objc
    private func dataInitializedHandler() {
        debugPrint("<EWS> ShortClipsPagingView -> dataInitializedHandler")
        
        let updatedShortClipsContainer = ShortClipsResource.manager.fetchedShortClipsContainer
        ShortClipsResource.manager.fetchedShortClipsContainer.removeAll()
        
        self.renderDataSource.removeAll()
        self.renderDataSource.append(contentsOf: updatedShortClipsContainer)
        
        self.renderDataSource = ShortClipsResource.manager.removeDuplicatedShortClips(from: self.renderDataSource)
        var indexPathsContainer = [IndexPath]()
        
        for row in (self.renderDataSource.count - updatedShortClipsContainer.count)..<self.renderDataSource.count {
            indexPathsContainer.append(IndexPath.init(row: row, section: 0))
        }
        
        self.shortClipsContainerView.insertRows(at: indexPathsContainer, with: UITableView.RowAnimation.none)
        
        self.shortClipsContainerView.reloadData()

        
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.shortClipsContainerView.scrollToRow(at: IndexPath(row: self.currentShortClipIndex, section: 0), at: UITableView.ScrollPosition.middle, animated: false)
            
            if self.isCurrentShortClipCellPaused == false {
                self.shortClipsPlayControl()
            }
            
//            self.shortClipsPlayControl()
            
        }
        
    }
    
    @objc
    private func pageNextHandler() {
        debugPrint("<EWS> ShortClipsPagingView -> pageNextHandler")
        
        guard let currentCell = self.shortClipsContainerView.cellForRow(at: IndexPath(row: self.currentShortClipIndex, section: 0)) as? ShortClipReusableCell else {
            return
        }
        
        currentCell.replay()
    }
    
    @objc
    private func applicationBecomeActive() {
        guard let currentCell = self.shortClipsContainerView.cellForRow(at: IndexPath(row: self.currentShortClipIndex, section: 0)) as? ShortClipReusableCell else {
            return
        }
        
        if self.isCurrentShortClipCellPaused == false {
            currentCell.play()
        }
    }
    
    @objc
    private func applicationEnterBackground() {
        guard let currentCell = self.shortClipsContainerView.cellForRow(at: IndexPath(row: self.currentShortClipIndex, section: 0)) as? ShortClipReusableCell else {
            return
        }
        
        self.isCurrentShortClipCellPaused = !currentCell.retrievePlayPauseStatus()
        currentCell.pause()
        
    }
    
    private func shortClipsPlayControl() {
        debugPrint("<EWS> ShortClipPagingView -> shortClipsPlayControl")

        guard let currentCell = self.shortClipsContainerView.cellForRow(at: IndexPath(row: self.currentShortClipIndex, section: 0)) as? ShortClipReusableCell
        else {
            debugPrint("<EWS> => Current Cell DNE")
            return
        }
        
//        weak var previousCell = self.shortClipsContainerView.cellForRow(at: IndexPath(row: self.previousShortClipIndex, section: 0)) as? ShortClipReusableCell
//        guard let previousCell =  previousCell else {
//            debugPrint("<EWS> => Previous Cell DNE")
//            return
//        }
        

        if currentCell.isPlayerReady == true {
            debugPrint("<EWS> => 01 (Video Ready & Replay)")
            currentCell.replay()
        } else {
            debugPrint("<EWS> => 02 (Loading Video)")
            ShortClipsPlayer.manager.pauseAll()
            
            currentCell.onPlayerReady = { [weak self] in
                debugPrint("<EWS> => 03 (Video Now Ready)")
                if let currentCellIndexPath = self?.shortClipsContainerView.indexPath(for: currentCell) {
//                    debugPrint("<EWS> => 04 (Retrieved Index)")
                    if self?.isCurrentShortClipCellPaused ?? true == false
                        && currentCellIndexPath.row == self?.currentShortClipIndex {
                        debugPrint("<EWS> => 05 (Index Matched & Play)")
                        currentCell.play()
                    } else {
                        debugPrint("<EWS> => 06 (Index Not Matched)")
                        debugPrint("<EWS> => 06 DisplayIndex: \(currentCellIndexPath.row)")
                        debugPrint("<EWS> => 06 Current Index: \(self?.currentShortClipIndex ?? -1)")
                        debugPrint("<EWS> => 06 Previous Index: \(self?.previousShortClipIndex ?? -1)")

//                        ShortClipsPlayer.manager.pauseAll()
//
//                        currentCell.play()
                        
                    }
                } else {
                    debugPrint("<EWS> 07 (Index Error)")
//                    currentCell.play()
//                    self?.shortClipsContainerView.reloadData()
                    
                }
            }
            
        }
    }

    
}

extension ShortClipsPagingView {
    public func reset() {
        self.currentShortClipIndex = 0
        self.renderDataSource.removeAll()
        
        NotificationCenter.default.removeObserver(self)
        self.removeObserver(self, forKeyPath: "currentShortClipIndex")
    }
}

// - MARK: Delegate Functions
extension ShortClipsPagingView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.renderDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.shortClipsContainerView.dequeueReusableCell(withIdentifier: self.reusableShortClipCellID) as? ShortClipReusableCell
        else {
            let emptyCell = UITableViewCell()
            emptyCell.backgroundColor = UIColor.red
            
            return emptyCell
        }
        
        let shortClipData = self.renderDataSource[indexPath.row]
        
        cell.updateCellData(
            title: shortClipData.title,
            shortClipID: shortClipData.shortClipID,
            videoURL: shortClipData.videoURL,
            imageURL: shortClipData.thumbnailImageURL
        )
        
        return cell
        
    }
    
}

extension ShortClipsPagingView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.frame.size.height
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        DispatchQueue.main.async {
            
            scrollView.panGestureRecognizer.isEnabled = false
            
            let translatedPoint = scrollView.panGestureRecognizer.translation(in: scrollView)
            
            if translatedPoint.y < -50 && self.currentShortClipIndex < (self.renderDataSource.count - 1) {
                self.previousShortClipIndex = self.currentShortClipIndex
                self.currentShortClipIndex += 1
            }
            if translatedPoint.y > 50 && self.currentShortClipIndex > 0 {
                self.previousShortClipIndex = self.currentShortClipIndex
                self.currentShortClipIndex -= 1
            }
            
            UIView.animate(withDuration: 0.35, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.shortClipsContainerView.scrollToRow(at: IndexPath(row: self.currentShortClipIndex, section: 0), at: UITableView.ScrollPosition.middle, animated: false)
            }, completion: { finished in
                scrollView.panGestureRecognizer.isEnabled = true
            })
            
        }
        
        
        
//        self.shortClipsContainerView.scrollToRow(at: IndexPath(row: self.currentShortClipIndex, section: 0), at: UITableView.ScrollPosition.middle, animated: false)

    }
}

// - MARK: observeValue
extension ShortClipsPagingView {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "currentShortClipIndex" {
            
            debugPrint("<EWS> ShortClipsPagingView -> observeValue")
            
            debugPrint("<EWS> => CurrentIndex: \(self.currentShortClipIndex), DataSourceSize: \(self.renderDataSource.count)")
            if self.currentShortClipIndex >= (self.renderDataSource.count - 5) {
                ShortClipsResource.manager.checkFetchedShortClipsContainerStatus(currentIndex: self.currentShortClipIndex)
            }
            
//            ShortClips.manager.checkFetchedShortClipsContainerStatus(currentIndex: self.currentShortClipIndex)

            self.isCurrentShortClipCellPaused = false
            
            self.shortClipsPlayControl()
            
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
}
