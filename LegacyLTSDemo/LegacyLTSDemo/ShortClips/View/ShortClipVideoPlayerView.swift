//
//  ShortClipVideoPlayerView.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/23/24.
//

import UIKit
import AVKit

protocol ShortClipVideoPlayerViewStatusUpdateDelegate: NSObjectProtocol {
    func onProgressUpdate(currentProgress: Float, duration: Float)
    func onPlayItemStatusUpdate(status: AVPlayerItem.Status)
}

class ShortClipVideoPlayerView: UIView {
    public var playerStatusUpdateDelegate: ShortClipVideoPlayerViewStatusUpdateDelegate?
    
    private var videoURL: URL?
    private var videoPlayer: ShortClipVideoPlayer?
    private var videoplayerLayer: AVPlayerLayer
    private var videoPlayerProgressObserver: Any?
    
    
    override init(frame: CGRect) {
        self.videoplayerLayer = AVPlayerLayer(player: self.videoPlayer?.retrievePlayer())
        
        super.init(frame: frame)
        
        self.setupSubview(frame: frame)
        self.setupPlayerProgressObserver()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.removePlayerProgressObserver()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.videoplayerLayer.frame = self.layer.bounds
        CATransaction.commit()
    }
}

extension ShortClipVideoPlayerView {
    private func setupSubview(frame: CGRect) {
        
        self.layer.addSublayer(self.videoplayerLayer)
        
    }
    
    private func updateShortClipOrientation(playerItem: AVPlayerItem) {
        let videoTracks = playerItem.asset.tracks(withMediaType: AVMediaType.video)
        
        if videoTracks.count > 0,
            let firstTrack = videoTracks.first {
            let videoSize = firstTrack.naturalSize
            
            if videoSize.height > videoSize.width {
                self.videoplayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            } else {
                self.videoplayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
            }
        }
        
    }
    
    private func setupPlayerProgressObserver() {
        self.videoPlayerProgressObserver = self.videoPlayer?.retrievePlayer().addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 1), queue: DispatchQueue.main, using: { [weak self] progressTime in
            
            let videoCurrentProgress = CMTimeGetSeconds(progressTime)
//            let videoDuration = CMTimeGetSeconds(self?.videoPlayerItem?.duration ?? CMTime())
            let videoDuration = CMTimeGetSeconds(self?.videoPlayer?.retrievePlayer().currentItem?.duration ?? CMTime())
            
            if videoCurrentProgress == videoDuration {
                self?.replay()
            }
            
            self?.playerStatusUpdateDelegate?.onProgressUpdate(
                currentProgress: Float(videoCurrentProgress),
                duration: Float(videoDuration))
            
        })
    }
    
    private func removePlayerProgressObserver() {
//        self.videoPlayerItem?.removeObserver(self, forKeyPath: "status")
        self.videoPlayer?.retrievePlayer().currentItem?.removeObserver(self, forKeyPath: "status")
        
        guard let videoPlayerProgressObserver = self.videoPlayerProgressObserver else {
            return
        }
        
        self.videoPlayer?.retrievePlayer().removeTimeObserver(videoPlayerProgressObserver)
    }
}

extension ShortClipVideoPlayerView {
    public func updatePlayerViewData(videoPath: String) {
        self.videoURL = URL(string: videoPath)
        
        if let cachedPlayer = ShortClipsPlayer.manager.retrievePreloadedPlayer(cacheID: videoPath) {
            debugPrint("<EWS> => Load From Cached ShortClipsPlayersContainer")
            
            self.videoPlayer = cachedPlayer

        } else {
            
            if let localURL = ShortClipsDownload.manager.retrieveDownloadedSessionURL(shortClipID: videoPath) {
                debugPrint("<EWS> => Load From Local URL")
                let localAsset = AVAsset(url: localURL)
                let localPlayerItem = AVPlayerItem(asset: localAsset)
                localPlayerItem.preferredForwardBufferDuration = 3.0
                
                let preloadedShortClipPlayer = ShortClipVideoPlayer(
                    playerItem: localPlayerItem,
                    playerID: videoPath
                )
                
                self.videoPlayer = preloadedShortClipPlayer
                
                ShortClipsPlayer.manager.append(preloadedPlayer: preloadedShortClipPlayer)
                
            } else {
                debugPrint("<EWS> => Load From Remote URL")

                if let videoURL = self.videoURL {
                    
                    let instantPlayerItem = AVPlayerItem(url: videoURL)
                    instantPlayerItem.preferredForwardBufferDuration = 3.0
                    
                    self.videoPlayer = ShortClipVideoPlayer(playerItem: instantPlayerItem, playerID: videoPath)
                } else {
                    debugPrint("<EWS> => URL Error")
                }
            }
            
        }

        
//        if let videoPlayerItem = self.videoPlayerItem {
        if let videoPlayerItem = self.videoPlayer?.retrievePlayer().currentItem {
            self.updateShortClipOrientation(playerItem: videoPlayerItem)
            
            videoPlayerItem.addObserver(
                self,
                forKeyPath: "status",
                options: [
                    NSKeyValueObservingOptions.old,
                    NSKeyValueObservingOptions.new
                ],
                context: nil
            )
        } else {
            debugPrint("<EWS> 404")
        }
        
//        self.videoPlayerItem?.addObserver(
//            self,
//            forKeyPath: "status",
//            options: [
//                NSKeyValueObservingOptions.old,
//                NSKeyValueObservingOptions.new
//            ],
//            context: nil
//        )
        
        // Player Layer
        self.videoplayerLayer.player = self.videoPlayer?.retrievePlayer()
        
        self.setupPlayerProgressObserver()
    }
    
    public func cancelLoading() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        self.videoplayerLayer.isHidden = true
        CATransaction.commit()
        
        self.removePlayerProgressObserver()
        self.pause()
        
        self.videoPlayer = nil
//        self.videoPlayerItem = nil
        self.videoplayerLayer.player = nil
        
        
        
    }
    
    public func togglePlayState() {
        if self.videoPlayer?.retrievePlayer().rate == 0 {
            self.play()
        } else {
            self.pause()
        }
    }
    
    public func currentPlaybackRate() -> CGFloat {
        let rate = CGFloat(self.videoPlayer?.retrievePlayer().rate ?? 0.0)
        
        return rate
    }
    
    public func play() {
        guard let videoPlayer = self.videoPlayer else {
            return
        }
        
        ShortClipsPlayer.manager.play(player: videoPlayer)
    }
    
    public func replay() {
        guard let videoPlayer = self.videoPlayer else {
            return
        }
        
        ShortClipsPlayer.manager.replay(player: videoPlayer)
    }
    
    public func pause() {
        guard let videoPlayer = self.videoPlayer else {
            return
        }
        
        ShortClipsPlayer.manager.pause(player: videoPlayer)
    }
}

// - MARK: observeValue
extension ShortClipVideoPlayerView {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status" {
//            if self.videoPlayerItem?.status == AVPlayerItem.Status.readyToPlay {
            if self.videoPlayer?.retrievePlayer().currentItem?.status == AVPlayerItem.Status.readyToPlay {
                CATransaction.begin()
                CATransaction.setDisableActions(true)
                self.videoplayerLayer.isHidden = false
                CATransaction.commit()
            }
            
//            self.playerStatusUpdateDelegate?.onPlayItemStatusUpdate(status: self.videoPlayerItem?.status ?? AVPlayerItem.Status.unknown)
            self.playerStatusUpdateDelegate?.onPlayItemStatusUpdate(status: self.videoPlayer?.retrievePlayer().currentItem?.status ?? AVPlayerItem.Status.unknown)
            
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
}

struct ShortClipVideoPlayer {
    private let playerID: String
    private let player: AVPlayer

    public init(playerItem: AVPlayerItem?, playerID: String) {
        self.playerID = playerID
        self.player = AVPlayer(playerItem: playerItem)
        
    }
    
    public func retrieveID() -> String {
        return self.playerID
    }
    
    public func retrievePlayer() -> AVPlayer {
        return self.player
    }
}
