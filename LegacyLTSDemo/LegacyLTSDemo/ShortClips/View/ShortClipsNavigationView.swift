//
//  ShortClipsNavigationView.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import UIKit
import WebKit

class ShortClipsNavigationView: UIView {
    private let viewController: ShortClipsNavigationViewController
    
    init(frame: CGRect, controller: ShortClipsNavigationViewController) {
        self.viewController = controller
        super.init(frame: frame)
        
        self.setupSubview(frame: frame)
        self.setupObservers()
        
        ShortClipsResource.manager.initialize()
        
//        ShortClipsWebView.manager.loadCookie()
//        self.establishWebViewRequest()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        
    }
    
    private lazy var shortClipsWebView: WKWebView = {
        let webViewConfig = WKWebViewConfiguration()
        
        webViewConfig.dataDetectorTypes = [WKDataDetectorTypes.all]
        
        let webView = WKWebView(
            frame: CGRect.zero,
            configuration: webViewConfig
        )
        
        webView.backgroundColor = UIColor.orange
        
        webView.navigationDelegate = self
        
        return webView
    }()
    
    private lazy var shortClipsPagingView: ShortClipsPagingView = {
        let view = ShortClipsPagingView(
            frame: self.frame,
            controller: self.viewController
        )
        
        // Debug Overlay
//        view.layer.borderWidth = 5
//        view.layer.borderColor = UIColor.red.cgColor
        
        return view
    }()
}

extension ShortClipsNavigationView {
    private func setupSubview(frame: CGRect) {
        self.addSubview(self.shortClipsWebView)
        AutoLayoutUI.manager.fillPredecessor(for: self.shortClipsWebView)
        
        self.addSubview(self.shortClipsPagingView)
        AutoLayoutUI.manager.placeEdges(
            for: self.shortClipsPagingView,
            padding: UIEdgeInsets(
                top: 0,
                left: 0,
                bottom: (self.shortClipsWebView.frame.size.height - (self.shortClipsWebView.frame.size.width * 0.12)),
//                bottom: 50,
                right: 0
            ),
            top: self.safeAreaLayoutGuide.topAnchor, 
            left: self.leftAnchor,
            bottom: self.shortClipsWebView.bottomAnchor,
            right: self.rightAnchor
        )
        
        
    }
    
    private func setupObservers() {
        
    }
    
    private func establishWebViewRequest() {
//        guard let requestURL = URL(string: "https://qa-www.mytvsuper.com/tc/scoopplus") else {
//            return
//        }
        
        guard let requestURL = URL(string: "https://scoopplus:Scoop_qa@qa-www.mytvsuper.com/tc/scoopplus") else {
            return
        }
        
        var request = URLRequest(
            url: requestURL,
            timeoutInterval: TimeInterval(10)
        )
        
        request.addValue("Basic c2Nvb3BwbHVzOlNjb29wX3Fh", forHTTPHeaderField: "Authorization")
        
        request.setValue("Mozilla/5.0 (iPhone; CPU iPhone OS 17_2_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 myTVSUPER/5.7.1 SuperApp", forHTTPHeaderField: "User-Agent")
        
        
        self.shortClipsWebView.load(request)
    }
}

extension ShortClipsNavigationView {
    public func resetShortClipsPagingView() {
        self.shortClipsPagingView.reset()
    }
}

extension ShortClipsNavigationView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        debugPrint("<EWS> ShortClipsNavigationView -> WKNavigationDelegate => didStartProvisionalNavigation")
        
        
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
//        debugPrint("<EWS> ShortClipsNavigationView -> WKNavigationDelegate => didFailProvisionalNavigation")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("<EWS> ShortClipsNavigationView -> WKNavigationDelegate => didFinish")
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            debugPrint("<EWS> ShortClipsNavigationView -> decidePolicyForNavigationAction => URL: \(url)")
        }
        
        if navigationAction.navigationType == WKNavigationType.reload {
            decisionHandler(WKNavigationActionPolicy.cancel)
        } else {
            decisionHandler(WKNavigationActionPolicy.allow)
        }
    }
    
//    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
//        
//        if let response = navigationResponse.response as? HTTPURLResponse {
//            let headers = response.allHeaderFields
//            //do something with headers
//            
//            debugPrint("<EWS> ShortClipsNavigationView -> decidePolicyForNavigationResponse => \(response.allHeaderFields)")
//        }
//        
//        decisionHandler(WKNavigationResponsePolicy.cancel)
//    }
}



