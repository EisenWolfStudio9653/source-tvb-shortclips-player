//
//  ShortClipsWebViewManager.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/23/24.
//

import UIKit
import WebKit

final class ShortClipsWebView {
    private init() {}
    public static let manager = ShortClipsWebView()
    
    private var preloadedShortClipWebViewsContainer = [WKWebView]()
}

extension ShortClipsWebView {
    public func saveCookie() {
        let cookieJar: HTTPCookieStorage = HTTPCookieStorage.shared
        if let cookies = cookieJar.cookies {
            let data: Data = NSKeyedArchiver.archivedData(withRootObject: cookies)
            let ud: UserDefaults = UserDefaults.standard
            ud.set(data, forKey: "cookie")
        }
    }
    
    public func loadCookie() {
        let ud: UserDefaults = UserDefaults.standard
        let data: Data? = ud.object(forKey: "cookie") as? Data
        if let cookie = data {
            let datas: NSArray? = NSKeyedUnarchiver.unarchiveObject(with: cookie) as? NSArray
            if let cookies = datas {
                for c in cookies {
                    if let cookieObject = c as? HTTPCookie {
                        HTTPCookieStorage.shared.setCookie(cookieObject)
                    }
                }
            }
        }
    }
}

extension ShortClipsWebView {
    private func preloadShortClipWebView(shortClipID: String) {
        let webViewConfig = WKWebViewConfiguration()
        
        let webView = WKWebView(
            frame: CGRect.zero,
            configuration: webViewConfig
        )
        
        guard let requestURL = URL(string: "https://scoopplus:Scoop_qa@qa-www.mytvsuper.com/tc/scoopplus/app-native-shorts/\(shortClipID)/") else {
            return
        }
    
        var request = URLRequest(
            url: requestURL,
            timeoutInterval: TimeInterval(10)
        )
        
        request.addValue(
            "Basic c2Nvb3BwbHVzOlNjb29wX3Fh",
            forHTTPHeaderField: "Authorization"
        )
        request.setValue(
            "Mozilla/5.0 (iPhone; CPU iPhone OS 17_2_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 myTVSUPER/5.7.1 SuperApp",
            forHTTPHeaderField: "User-Agent"
        )
        
        webView.load(request)
    }
}





class ShortClipsWebViewController: UIViewController {
    private let authURLPath = "https://scoopplus:Scoop_qa@qa-www.mytvsuper.com/tc/scoopplus/shorts"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}

extension ShortClipsWebViewController {
    
}
