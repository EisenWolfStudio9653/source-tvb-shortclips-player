//
//  AutoLayoutUIManager.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import UIKit

final class AutoLayoutUI {
    private init() {}
    public static let manager = AutoLayoutUI()
    
    private struct LayoutConstraintProperties {
        var top: NSLayoutConstraint?
        var left: NSLayoutConstraint?
        var bottom: NSLayoutConstraint?
        var right: NSLayoutConstraint?
        
        var x: NSLayoutConstraint?
        var y: NSLayoutConstraint?
        var width: NSLayoutConstraint?
        var height: NSLayoutConstraint?
    }
    
    public enum LayoutXAxis {
        case left, center, right
    }
    
    public enum LayoutYAxis {
        case top, center, bottom
    }
}

extension AutoLayoutUI {
    /**
     Function to layout a view component to fill its predecessor view.
     
     - Important: Check for invalid / nil predecessor view.
     
     - Parameters:
        - for: The view component to be set layout.
        - padding: Optional spacing around the edges (should always be positive values).
     */
    public func fillPredecessor(
        for targetView: UIView,
        padding: UIEdgeInsets = UIEdgeInsets.zero
    )  {
        guard let predecessorViewTopAnchor = targetView.superview?.topAnchor,
                let predecessorViewLeftAnchor = targetView.superview?.leftAnchor,
                let predecessorViewBottomAnchor = targetView.superview?.bottomAnchor,
                let predecessorViewRightAnchor = targetView.superview?.rightAnchor
        else {
            assert(false, "<AutoLayoutUIManager> PredecessorVIew Error")
            return
        }
        
        var layoutConstrains = AutoLayoutUI.LayoutConstraintProperties()
        
        targetView.translatesAutoresizingMaskIntoConstraints = false
        
        layoutConstrains.top = targetView.topAnchor.constraint(
            equalTo: predecessorViewTopAnchor,
            constant: padding.top
        )
        layoutConstrains.left = targetView.leftAnchor.constraint(
            equalTo: predecessorViewLeftAnchor,
            constant: padding.left
        )
        layoutConstrains.bottom = targetView.bottomAnchor.constraint(
            equalTo: predecessorViewBottomAnchor,
            constant: (-padding.bottom)
        )
        layoutConstrains.right = targetView.rightAnchor.constraint(
            equalTo: predecessorViewRightAnchor,
            constant: (-padding.right)
        )
        
        
        [
            layoutConstrains.top, 
            layoutConstrains.left,
            layoutConstrains.bottom,
            layoutConstrains.right
        ].forEach {
            $0?.isActive = true
        }
    }
    
    /**
     Function to layout a view component by placing its axis.
     
     - Important: Offset values could be negative values relatively.
     
     - Parameters:
        - for: The view component to be set layout.
        - viewSize: The size (width & height) of the view component.
        - x: The relative X axis (left, center, right) for layout.
        - xAnchor: The layout anchor on X axis.
        -   xOffset: The offset value of the X layout anchor.
        -   y: The relative y axis (top, center, bottom) for layout.
        -   yAnchor: The layout anchor on Y axis.
        -   yOffset: The offset value of the Y layout anchor.
     */
    public func placeAxises(
        for targetView: UIView,
        viewSize size: CGSize, 
        x: AutoLayoutUI.LayoutXAxis,
        xAnchor: NSLayoutXAxisAnchor,
        xOffset: CGFloat,
        y: AutoLayoutUI.LayoutYAxis,
        yAnchor: NSLayoutYAxisAnchor,
        yOffset: CGFloat
    ) {
        var layoutConstrains = AutoLayoutUI.LayoutConstraintProperties()
        
        targetView.translatesAutoresizingMaskIntoConstraints = false
        
        switch x {
        case .left:
            layoutConstrains.x = targetView.leftAnchor.constraint(
                equalTo: xAnchor,
                constant: xOffset
            )
        case .center:
            layoutConstrains.x = targetView.centerXAnchor.constraint(
                equalTo: xAnchor,
                constant: xOffset
            )
        case .right:
            layoutConstrains.x = targetView.rightAnchor.constraint(
                equalTo: xAnchor,
                constant: xOffset
            )
        }
        
        switch y {
        case .top:
            layoutConstrains.y = targetView.topAnchor.constraint(
                equalTo: yAnchor,
                constant: yOffset
            )
        case .center:
            layoutConstrains.y = targetView.centerYAnchor.constraint(
                equalTo: yAnchor,
                constant: yOffset
            )
        case .bottom:
            layoutConstrains.y = targetView.bottomAnchor.constraint(
                equalTo: yAnchor,
                constant: yOffset
            )
        }
        
        layoutConstrains.width = targetView.widthAnchor.constraint(equalToConstant: size.width)
        layoutConstrains.height = targetView.heightAnchor.constraint(equalToConstant: size.height)
        
        [
            layoutConstrains.x,
            layoutConstrains.y,
            layoutConstrains.width,
            layoutConstrains.height
        ].forEach {
            $0?.isActive = true
        }
    }
    
    /**
     Function to layout a view component by placing its edges.
     
     - Important:
     
     - Parameters:
        - for: The view component to be set layout.
        -   padding: Optional spacing around the edges (should always be positive values).
        -   top: The layout anchor on the top edge.
        -   left: The layout anchor on the left edge.
        -   bottom: The layout anchor on the bottom edge.
        -   right: The layout anchor on the right edge.
     */
    public func placeEdges(
        for targetView: UIView,
        padding: UIEdgeInsets = UIEdgeInsets.zero,
        top: NSLayoutYAxisAnchor,
        left: NSLayoutXAxisAnchor,
        bottom: NSLayoutYAxisAnchor,
        right: NSLayoutXAxisAnchor
    ) {
        var layoutConstrains = AutoLayoutUI.LayoutConstraintProperties()
        
        targetView.translatesAutoresizingMaskIntoConstraints = false
        
        layoutConstrains.top = targetView.topAnchor.constraint(
            equalTo: top,
            constant: padding.top
        )
        layoutConstrains.left = targetView.leftAnchor.constraint(
            equalTo: left,
            constant: padding.left
        )
        layoutConstrains.bottom = targetView.bottomAnchor.constraint(
            equalTo: bottom,
            constant: (-padding.bottom)
        )
        layoutConstrains.right = targetView.rightAnchor.constraint(
            equalTo: right,
            constant: (-padding.right)
        )
        
        [
            layoutConstrains.top,
            layoutConstrains.left,
            layoutConstrains.bottom,
            layoutConstrains.right
        ].forEach {
            $0?.isActive = true
        }
    }
    
}



// - TODO: Refecgtor
extension AutoLayoutUI {
    public func decodeRGBHexString(_ hex: String) -> UIColor {
        var colorString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (colorString.hasPrefix("#")) {
            colorString.remove(at: colorString.startIndex)
        }

        if ((colorString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: colorString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
