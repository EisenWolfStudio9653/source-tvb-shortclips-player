//
//  HomePageViewController.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import UIKit

class HomePageViewController: UIViewController {
    
    private var controllerView: HomePageView?
    private var controllerModel: HomePageModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Home"
        self.navigationItem.largeTitleDisplayMode = UINavigationItem.LargeTitleDisplayMode.never
        
        self.controllerView = HomePageView(frame: self.view.frame, controller: self)
        self.controllerModel = HomePageModel(viewController: self)

        guard let controllerView = self.controllerView else {
            return
        }
        
        self.view = controllerView
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        debugPrint("<EWS> HomePageViewController -> viewDidDisappear")
        self.controllerView = nil
        self.controllerModel = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        debugPrint("<EWS> HomePageViewController -> viewDidAppear")
        
    }


}

extension HomePageViewController {
    public func navigateToShortClipsDemo() {
        self.navigationController?.pushViewController(ShortClipsNavigationViewController(), animated: true)
        
    }
    
    public func navigateToMultiCamDemo() {
        debugPrint("<EWS> navigateToMultiCamDemo()")
//        self.navigationController?.pushViewController(ShortClipsNavigationViewController(), animated: true)
    }
    
    public func navigateToWebViewQALogin() {
        self.navigationController?.pushViewController(WebViewQALoginViewController(), animated: true)
        
    }
}

