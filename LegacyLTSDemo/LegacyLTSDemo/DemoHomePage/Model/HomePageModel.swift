//
//  HomePageModel.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import Foundation

class HomePageModel {
    private let viewController: HomePageViewController
    
    init(viewController: HomePageViewController) {
        self.viewController = viewController
    }
}

extension HomePageModel {
    
}
