//
//  HomePageView.swift
//  LegacyLTSDemo
//
//  Created by EWS EisenWolfStudio on 2/7/24.
//

import UIKit

class HomePageView: UIView {
    
    private let viewController: HomePageViewController
    
    init(frame: CGRect, controller: HomePageViewController) {
        self.viewController = controller
        super.init(frame: frame)
        
        self.setupSubview(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var shortClipsDemoButton: UIButton = {
        let button = UIButton()
        
        button.backgroundColor = UIColor.orange
        button.tintColor = UIColor.white
        button.layer.cornerRadius = 5
        button.setTitle("Short Clips", for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.shortClipsDemoButtonHandler), for: UIControl.Event.touchUpInside)
        
        return button
    }()
    
    private lazy var multiCamDemoButton: UIButton = {
        let button = UIButton()
        
        button.backgroundColor = UIColor.orange
        button.tintColor = UIColor.white
        button.layer.cornerRadius = 5
        button.setTitle("Multiple Camera", for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.multiCamDemoButtonHandler), for: UIControl.Event.touchUpInside)
        
        return button
    }()
    
    private lazy var webViewQALoginButton: UIButton = {
        let button = UIButton()
        
        button.backgroundColor = UIColor.orange
        button.tintColor = UIColor.white
        button.layer.cornerRadius = 5
        button.setTitle("QA Auth", for: UIControl.State.normal)
        button.addTarget(self, action: #selector(self.webViewQALoginButtonHandler), for: UIControl.Event.touchUpInside)
        
        return button
    }()
}

extension HomePageView {
    @objc
    private func shortClipsDemoButtonHandler() {
        self.viewController.navigateToShortClipsDemo()
    }
    
    @objc
    private func multiCamDemoButtonHandler() {
        self.viewController.navigateToMultiCamDemo()
    }
    
    @objc
    private func webViewQALoginButtonHandler() {
        self.viewController.navigateToWebViewQALogin()
    }
}

extension HomePageView {
    
    private func setupSubview(frame: CGRect) {
        self.backgroundColor = UIColor.black
        
        self.addSubview(self.shortClipsDemoButton)
        AutoLayoutUI.manager.placeAxises(
            for: self.shortClipsDemoButton,
            viewSize: CGSize(
                width: frame.size.width * 0.8,
                height: frame.size.height * 0.1
            ),
            x: AutoLayoutUI.LayoutXAxis.center,
            xAnchor: self.centerXAnchor,
            xOffset: 0,
            y: AutoLayoutUI.LayoutYAxis.center,
            yAnchor: self.centerYAnchor,
            yOffset: -60
        )
        
        self.addSubview(self.multiCamDemoButton)
        AutoLayoutUI.manager.placeAxises(
            for: self.multiCamDemoButton,
            viewSize: CGSize(
                width: frame.size.width * 0.8,
                height: frame.size.height * 0.1
            ),
            x: AutoLayoutUI.LayoutXAxis.center,
            xAnchor: self.centerXAnchor,
            xOffset: 0,
            y: AutoLayoutUI.LayoutYAxis.top,
            yAnchor: self.shortClipsDemoButton.bottomAnchor,
            yOffset: frame.size.height * 0.05
        )
        
        self.addSubview(self.webViewQALoginButton)
        AutoLayoutUI.manager.placeAxises(
            for: self.webViewQALoginButton,
            viewSize: CGSize(
                width: frame.size.width * 0.8,
                height: frame.size.height * 0.1
            ),
            x: AutoLayoutUI.LayoutXAxis.center,
            xAnchor: self.centerXAnchor,
            xOffset: 0,
            y: AutoLayoutUI.LayoutYAxis.top,
            yAnchor: self.multiCamDemoButton.bottomAnchor,
            yOffset: frame.size.height * 0.05
        )
        
        
    }
}
